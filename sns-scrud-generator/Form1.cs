﻿using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace sns_scrud_generator
{
    public partial class Form1 : Form
    {
        private string _originFile;
        private string _destinyFolder;
        private Dictionary<TypeMessage, Color> _colors;
        public Form1()
        {
            InitializeComponent();
            _originFile = txtOrigin.Text;
            _destinyFolder = txtDestiny.Text;
            loadColors();
        }
        private void loadColors()
        {
            _colors = new Dictionary<TypeMessage, Color>();
            _colors.Add(TypeMessage.Error, Color.DarkRed);
            _colors.Add(TypeMessage.Warning, Color.DarkGoldenrod);
            _colors.Add(TypeMessage.Information, Color.DarkBlue);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtMessages.Text = txtDestiny.Text = txtOrigin.Text = string.Empty;
            _originFile = string.Empty;
        }

        private void btnExaminarOrigen_Click(object sender, EventArgs e)
        {
            if (openOrigin.ShowDialog() == DialogResult.OK)
            {
                _originFile = openOrigin.FileName;
                txtOrigin.Text = _originFile;
            }
        }

        private void btnDestinySearch_Click(object sender, EventArgs e)
        {
            DialogResult result = openDestiny.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(openDestiny.SelectedPath))
            {
                _destinyFolder = openDestiny.SelectedPath;
                txtDestiny.Text = _destinyFolder;
            }
        }

        private void txtOrigin_TextChanged(object sender, EventArgs e)
        {
            _originFile = txtOrigin.Text;
        }

        private void txtDestiny_TextChanged(object sender, EventArgs e)
        {
            _destinyFolder = txtDestiny.Text;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            ShowMessage(TypeMessage.Information, "Iniciando la generacion...");
            ShowMessage(TypeMessage.Information, string.Concat("Cargando Archivo '", _originFile, "'"));
            Scruds myScruds = DeserializeScrud(loadFile(_originFile));
            foreach (var sc in myScruds.ScrudList)
            {
                ShowMessage(TypeMessage.Information, string.Concat("Asegurando existencia de carpetas destino."));
                createDestinyFolder(_destinyFolder);
                ShowMessage(TypeMessage.Information, string.Concat("Generando archivos DB y DataAccess de: ", sc.Backend.MyTable.Name));
                generateBackendFiles(_destinyFolder, sc.Backend, sc.Type);

                ShowMessage(TypeMessage.Information, string.Concat("Generando archivos BussinesLogic y ServiceLayer de: ", sc.Backend.MyInternalClass.Name));
                generateServiceFiles(_destinyFolder, sc);

            }
            ShowMessage(TypeMessage.Information, string.Concat("Total de Scruds generados:", myScruds.ScrudList.Count));



        }

        private void createDestinyFolder(string destinyFolder)
        {
            if (!Directory.Exists(destinyFolder))
            {
                Directory.CreateDirectory(destinyFolder);
            }
        }

        private void generateServiceFiles(string destinyPath, Scrud sc)
        {
            createDestinyFolder(destinyPath);
            Directory.Delete(string.Concat(destinyPath, @"\Backend\"), true);
            generateControllerScript(destinyPath, sc);
            generateDataContractScript(destinyPath, sc);
            generateDataServiceScript(destinyPath, sc);

        }

        private void generateDataServiceScript(string destinyPath, Scrud sc)
        {
            //Cargar Template
            string spTemplate = loadFile(string.Concat(destinyPath, @"\Templates\ServiceContract_template.txt"));
            //Cargar NombreClase
            spTemplate = spTemplate.Replace("{$ClassName}", sc.Backend.MyInternalClass.Name);
            FileSaver(string.Concat(destinyPath, @"\Backend\ServiceContract_", sc.Backend.MyInternalClass.Name, ".cs"), spTemplate);

        }

        private void generateControllerScript(string destinyPath, Scrud sc)
        {
            //Cargar Template
            string spTemplate = loadFile(string.Concat(destinyPath, @"\Templates\BussinesLogicController_Template.txt"));
            //Cargar NombreClase
            spTemplate = spTemplate.Replace("{$ClassName}", sc.Backend.MyInternalClass.Name);
            var att = sc.DataContract.Attribs;
            var fields = sc.Backend.MyInternalClass.Fields;

            string strParametersData = BuildList(new ArrayList(att.ToArray()), (n) =>
            {
                var attribute = (Attrib)n;
                return string.Concat("data.", attribute.Name, ",");
            });
            spTemplate = spTemplate.Replace("{$DataParameters}", strParametersData);

            string strAsigments = BuildList(new ArrayList(att.ToArray()), (n) =>
            {
                var attribute = (Attrib)n;
                return string.Concat("\t\t\t\t",attribute.Name, " = data.",attribute.BackendField,",");
            });
            spTemplate = spTemplate.Replace("{$AsigmentToData}", strAsigments);

            strAsigments = BuildList(new ArrayList(att.ToArray()), (n) =>
            {
                var attribute = (Attrib)n;
                return string.Concat("\t\t\t\t", attribute.BackendField, " = data.", attribute.Name, ",");
            });
            spTemplate = spTemplate.Replace("{$AsigmentToService}", strAsigments);
            
            FileSaver(string.Concat(destinyPath, @"\Backend\", sc.Backend.MyInternalClass.Name, "Controller.cs"), spTemplate);

        }

        private void generateDataContractScript(string destinyPath, Scrud sc)
        {
            //Cargar Template
            string spTemplate = loadFile(string.Concat(destinyPath, @"\Templates\DataContract_Template.txt"));
            //Cargar NombreClase
            spTemplate = spTemplate.Replace("{$ClassName}", sc.Backend.MyInternalClass.Name);
            //Cargar Listas de Campos
            var privateMembers = sc.Backend.MyInternalClass.Fields;

            string strAttribs = BuildList(new ArrayList(privateMembers.ToArray()), (n) =>
            {
                var fi = (Field)n;
                return string.Concat("\t\tpublic ", fi.Type, " ", fi.Name, " { get; set; } ");
            });
            spTemplate = spTemplate.Replace("{$DataMemberList}", strAttribs);
            FileSaver(string.Concat(destinyPath, @"\Backend\", sc.Backend.MyInternalClass.Name, "Data.cs"), spTemplate);
        }

        private void generateBackendFiles(string destinyPath, Backend backend, string type)
        {
            
            //Directory.Delete(string.Concat(destinyPath, @"\Backend\"), true);
            generateTableScript(destinyPath, backend, type);
            generateSPScript(destinyPath, backend, type);
            generateDataAccessClassScript(destinyPath, backend, type);
            generateDataAccessManagerScript(destinyPath, backend, type);


        }

        private void generateDataAccessManagerScript(string destinyPath, Backend backend, string type)
        {
            //Cargar Template
            string spTemplate = loadFile(string.Concat(destinyPath, @"\Templates\DataAccessManager_template.txt"));
            //Cargar NombreTabla
            spTemplate = spTemplate.Replace("{$ClassName}", backend.MyInternalClass.Name);
            spTemplate = spTemplate.Replace("{$NombreTabla}", backend.MyTable.Name);
            //Cargar Listas de Campos
            var privateMembers = backend.MyInternalClass.Fields;
            /*      {$FieldList}*/
            int i = 0;
            string strFields = BuildList(new ArrayList(privateMembers.ToArray()), (n) =>
                 {

                     var fi = (Field)n;
                     string result = fi.Type.Equals("bool") ?
                                string.Concat("\t\t\t\trow[", i.ToString(), "].ToString() == \"S\",") :
                                string.Concat("\t\t\t\trow[", i.ToString(), "].ToString(),");
                     i++;
                     return result;
                 });
            spTemplate = spTemplate.Replace("{$FieldList}", strFields);

            /*      {$ParseFieldList}*/
            string strParsers = BuildList(new ArrayList(privateMembers.ToArray()), (n) =>
                {
                    var fi = (Field)n;

                    return string.Concat("\t\t\tdataAccess.AdParameter(\"@i_", fi.ColumnName, "\", DBTypeEnum.VarChar, data.",fi.Name,"); ");
                });
            spTemplate = spTemplate.Replace("{$ParseFieldList}", strParsers);
            FileSaver(string.Concat(destinyPath, @"\Backend\", backend.MyInternalClass.Name, "Manager.cs"), spTemplate);


        }

        private void generateDataAccessClassScript(string destinyPath, Backend backend, string type)
        {
            //Cargar Template
            string spTemplate = loadFile(string.Concat(destinyPath, @"\Templates\DataAccessClass_template.txt"));
            //Cargar NombreTabla
            spTemplate = spTemplate.Replace("{$ClassName}", backend.MyInternalClass.Name);
            //Cargar Listas de Campos
            var privateMembers = backend.MyInternalClass.Fields;
            /*      Miembros Privados*/
            string strPrivateMembers = BuildList(new ArrayList(privateMembers.ToArray()), (n) =>
                {
                    var fi = (Field)(n);
                    return string.Concat("\t\tprivate", " ", fi.Type, " ", fi.Name, "; ");

                });
            spTemplate = spTemplate.Replace("{$PrivateMembers}", strPrivateMembers);
            /*      Parametros*/
            string strParams = BuildList(new ArrayList(privateMembers.ToArray()), (n) =>
            {
                var fi = (Field)n;
                return string.Concat("\t\t\t\t\t\t", fi.Type, " ", fi.Name.ToLower(),",");
            });
            spTemplate = spTemplate.Replace("{$ConstructParam}", strParams);
            /*      {$Asigment}*/
            string strAssign = BuildList(new ArrayList(privateMembers.ToArray()), (n) =>
            {
                var fi = (Field)n;
                return string.Concat("\t\t\t_", fi.Name.ToLower(), " = ", fi.Name.ToLower(), "; ");
            }); 
            spTemplate = spTemplate.Replace("{$Asigment}", strAssign);
            /*      {$PublicAttrib}*/
            string strAttribs = BuildList(new ArrayList(privateMembers.ToArray()), (n) =>
                {
                    var fi = (Field)n;
                    return string.Concat("\t\tpublic ", fi.Type, " ", fi.Name," { get=> _", fi.Name.ToLower(), "; set => _", fi.Name.ToLower()," = value; } ");
                });
            spTemplate = spTemplate.Replace("{$PublicAttrib}", strAttribs);
            FileSaver(string.Concat(destinyPath, @"\Backend\", backend.MyInternalClass.Name, ".cs"), spTemplate);


        }

        private void generateSPScript(string destinyPath, Backend backend, string type)
        {
            //Cargar Template
            string spTemplate = loadFile(string.Concat(destinyPath, @"\Templates\sp_template.txt"));
            //Cargar NombreTabla
            spTemplate = spTemplate.Replace("{$NombreTabla}", backend.MyTable.Name);
            //Cargar NombreSP
            spTemplate = spTemplate.Replace("{$NombreSP}", string.Concat("sp_", backend.MyTable.Name));
            //Cargar Fecha
            spTemplate = spTemplate.Replace("{$TODAY}", DateTime.Now.ToString("d/M/yyyy"));
            //Cargar EsActivo
            spTemplate = spTemplate.Replace("{$EsActivo}", "EsActivo = 1");
            //Cargar Listas de campos
            var campos = from e in backend.MyTable.Columns select e;
            string strCampos = BuildList(new ArrayList(campos.ToArray()), (n) =>
            {
                var col = (Column)n;
                return string.Concat("\t\t", col.Type.Equals("bool") ?
                    string.Concat("case ", col.Name, " when 1 then 'S' else 'N' end as ", col.Name)
                    : col.Name, ",");
            });
            spTemplate = spTemplate.Replace("{$CampoSelectList}", strCampos);
            string strParameters = BuildList(new ArrayList(campos.ToArray()), (n) =>
            {
                var col = (Column)n;
                string typo = string.Empty;
                string len = string.Empty;
                string defaultValue = string.Empty;
                switch (col.Type)
                {
                    case "bool":
                        typo = "varchar(1)";
                        defaultValue = " = 'N'";
                        break;
                    case "varchar":
                        defaultValue = " = ''";
                        typo = col.Type;
                        break;
                    default:
                        defaultValue = " = 0";
                        typo = col.Type;
                        break;
                }

                if (string.IsNullOrWhiteSpace(col.Lenght))
                {
                    len = "";
                }
                else
                    len = string.Concat("(", col.Lenght, ")");

                return string.Concat("  ,@i_", col.Name.Equals(string.Concat("Codigo",backend.MyTable.Name))?"Codigo": col.Name, "\t\t", typo, len, "\t", defaultValue, " ");

            });
            spTemplate = spTemplate.Replace("{$InputParameterList}", strParameters);
            string strFields = BuildList(new ArrayList(campos.ToArray()), (n) =>
            {
                var col = (Column)n;
                return string.Concat("\t\t\t", col.Name, ",");
            });
            spTemplate = spTemplate.Replace("{$FieldList}", strFields);
            string strInsertFields = BuildList(new ArrayList(campos.ToArray()), (n) =>
            {
                var col = (Column)n;
                string column = string.Concat("@i_",col.Name);
                if (string.Concat("@i_",col.Name).Equals(string.Concat("@i_", "Codigo", backend.MyTable.Name)))
                    column = "@i_Codigo";
                if (col.Type.Equals("bool"))
                    if (col.Name.Equals("@i_EsActivo"))
                        column = "1";
                    else
                        column = string.Concat("case @i_", col.Name, " when 'S' then 1 else 0 end");
                
                return string.Concat("\t\t\t", column,",");
            });
            spTemplate = spTemplate.Replace("{$InsertList}", strInsertFields);

            campos = from e in backend.MyTable.Columns where !e.Name.Equals("EsActivo") && !e.Name.Equals(string.Concat("Codigo", backend.MyTable.Name)) select e;
            string strUpdateFields = BuildList(new ArrayList(campos.ToArray()), (n) =>
            {
                var col = (Column)n;
                string result = string.Empty;
                result = string.Concat("\t\t\t",col.Name, " = @i_",col.Name,",");
                return result;
            });
            spTemplate = spTemplate.Replace("{$UpdateList}", strUpdateFields);
            //Cargar ComparativoCodigo
            spTemplate = spTemplate.Replace("{$ComparativoCodigo}", string.Concat("Codigo",backend.MyTable.Name," = @i_Codigo"));
            FileSaver(string.Concat(destinyPath, @"\Backend\sp_", backend.MyTable.Name, ".sql"), spTemplate);
            
        }
        public bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }
        private void generateTableScript(string destinyPath, Backend backend, string type)
        {
            //Cargar Template
            string tableTemplate = loadFile(string.Concat(destinyPath, @"\Templates\Table_template.txt"));
            //Cargar NombreTabla
            tableTemplate = tableTemplate.Replace("{$NombreTabla}", backend.MyTable.Name);
            //Cargar Indices
            var indices = from e in backend.MyTable.Columns where e.IsIndex select e;
            string strIndices = string.Empty;
            strIndices = BuildList(new ArrayList(indices.ToArray()), (n) => 
                    {
                        var col = (Column)n;
                        return string.Concat(col.Name, "\t asc,");
                    });
            tableTemplate = tableTemplate.Replace("{$ListaIndice}", strIndices);
            //Carga de columnas
            string strColumnas = string.Empty;
            strColumnas = BuildList(new ArrayList(backend.MyTable.Columns.ToArray()), (n) =>
            {
                var col = (Column)n;
                string typo = string.Empty;
                string len = string.Empty;
                
                switch (col.Type)
                {
                    case "bool":
                        typo = "varchar(1)";
                        
                        break;
                    case "varchar":
                
                        typo = col.Type;
                        break;
                    default:
                
                        typo = col.Type;
                        break;
                }
                
                if (string.IsNullOrWhiteSpace(col.Lenght))
                {
                    len = "";
                }
                else
                    len = string.Concat("(", col.Lenght, ")");

                return string.Concat("\t", col.Name, "\t\t", typo, len,col.Nullable?" NULL":" NOT NULL",",");

            });
            tableTemplate = tableTemplate.Replace("{$ListaColumnas}", strColumnas);
            System.IO.Directory.CreateDirectory(string.Concat(destinyPath, @"\Backend\"));
            FileSaver(string.Concat(destinyPath, @"\Backend\Table_", backend.MyTable.Name, ".sql"), tableTemplate);

        }

        private string BuildList(ArrayList list, Func<object,string> AddLine)
        {
            string resp = string.Empty;
            foreach(object item in list)
            {
                resp = string.Concat(resp, "\n", AddLine(item));
            }
            resp = resp.Substring(0, resp.Length - 1);
            return resp;
        }
        
        
        private void ShowMessage(TypeMessage typeMessage, string message )
        {
            message = string.Concat("\n", message);
            txtMessages.SelectionStart = txtMessages.TextLength;
            txtMessages.SelectionLength = 0;
            txtMessages.SelectionColor = _colors[typeMessage];
            txtMessages.AppendText(message);
            txtMessages.SelectionColor = txtMessages.ForeColor;
        }
        private string SerializeScrud(Scruds scruds)
        {
            System.Xml.Serialization.XmlSerializer Serializador = new System.Xml.Serialization.XmlSerializer(scruds.GetType());
            StringWriter stream = new StringWriter();
            Serializador.Serialize(stream, scruds);
            return stream.ToString();
        }
        
        private Scruds DeserializeScrud(string strScruds)
        {
            var data = new Scruds();
            System.Xml.Serialization.XmlSerializer Serializador = new System.Xml.Serialization.XmlSerializer(data.GetType());
            StringReader stream = new StringReader(strScruds);
           return (Scruds)Serializador.Deserialize(stream);
        }
        private void FileSaver(string file, string Text)
        {
            try
            {
                StreamWriter sw = new System.IO.StreamWriter(file, true);
                sw.WriteLine(Text);
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private string loadFile(string path)
        {
            string[] lineas = File.ReadAllLines(path);
            string result = string.Empty;
            foreach (string linea in lineas)
            {

                result = string.Concat(result,linea,"\n");

            }
            return result;
        }
    }

    public enum TypeMessage
    {
        Error,
        Warning,
        Information
    }

   

}
