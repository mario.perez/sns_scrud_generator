﻿namespace sns_scrud_generator
{
    public class Field
    {
        private string _name;
        private string _type;
        private string _lenght;
        private string _columnName;

        public string Name { get => _name; set => _name = value; }
        public string Type { get => _type; set => _type = value; }
        public string Lenght { get => _lenght; set => _lenght = value; }
        public string ColumnName { get => _columnName; set => _columnName = value; }

        
    }
}
