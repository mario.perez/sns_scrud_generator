﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sns_scrud_generator
{
    public class DataContract
    {
        private List<Attrib> _attribs;

        public List<Attrib> Attribs { get => _attribs; set => _attribs = value; }
    }
}
