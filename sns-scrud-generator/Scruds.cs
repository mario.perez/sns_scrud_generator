﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sns_scrud_generator
{
    public class Scruds
    {
        private List<Scrud> _scrudList = new List<Scrud>();

        public List<Scrud> ScrudList { get => _scrudList; set => _scrudList = value; }
    }
}
