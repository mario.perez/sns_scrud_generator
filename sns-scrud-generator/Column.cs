﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sns_scrud_generator
{
    public class Column
    {
        private string _name;
        private string _type;
        private string _lenght;
        private bool _nullable;
        private bool _isIndex;


        public string Name { get => _name; set => _name = value; }
        public string Type { get => _type; set => _type = value; }
        public string Lenght { get => _lenght; set => _lenght = value; }
        public bool Nullable { get => _nullable; set => _nullable = value; }
        public bool IsIndex { get => _isIndex; set => _isIndex = value; }
    }
}
