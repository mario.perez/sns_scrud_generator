﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sns_scrud_generator
{
    public class Backend
    {
        private Table _myTable;
        private InternalClass _myInternalClass;

        public Table MyTable { get => _myTable; set => _myTable = value; }
        public InternalClass MyInternalClass { get => _myInternalClass; set => _myInternalClass = value; }
    }
}
