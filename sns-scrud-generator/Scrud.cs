﻿namespace sns_scrud_generator
{
    public class Scrud
    {
        private string _type;
        private Backend _backend;
        private DataContract _dataContract;

        public string Type { get => _type; set => _type = value; }
        public Backend Backend { get => _backend; set => _backend = value; }
        public DataContract DataContract { get => _dataContract; set => _dataContract = value; }
    }
}
