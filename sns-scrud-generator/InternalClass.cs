﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sns_scrud_generator
{
    public class InternalClass
    {
        private string _name;
        private List<Field> _fields;

        public string Name { get => _name; set => _name = value; }
        public List<Field> Fields { get => _fields; set => _fields = value; }
    }
}
