﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sns_scrud_generator
{
    public class Attrib
    {
        private string _name;
        private string _backendField;

        public string Name { get => _name; set => _name = value; }
        public string BackendField { get => _backendField; set => _backendField = value; }
    }
}
