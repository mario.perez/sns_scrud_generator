﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sns_scrud_generator
{
    public class Table
    {
        private string _name;
        private List<Column> _columns;

        public string Name { get => _name; set => _name = value; }
        public List<Column> Columns { get => _columns; set => _columns = value; }


    }
}
